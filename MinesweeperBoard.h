#ifndef UNTITLED5_MINESWEEPERBOARD_H
#define UNTITLED5_MINESWEEPERBOARD_H
#include <iostream>
enum GameMode  { DEBUG, EASY, NORMAL, HARD };
enum GameState { RUNNING, FINISHED_WIN, FINISHED_LOSS };
struct Field
{
    bool hasMine;
    bool hasFlag;
    bool isRevealed;
};

class MinesweeperBoard {
	Field board[100][100];
    int width;
    int height;
	GameMode level;
	GameState state;
	int mines;
public:
    MinesweeperBoard(int x, int y, GameMode mode);
    void debug_display() const;
	int getBoardWidth() const;
	int getBoardHeight() const;
	int getMineCount() const;
	int countMines(int x, int y) const;
	bool hasFlag(int x, int y) const;
	bool isRevealed(int x, int y)const;
	void toogleFlag(int x, int y);
	void revealField(int x, int y);
	GameState getGameState() const;
	char getFieldInfo(int x, int y) const;

};

#endif
