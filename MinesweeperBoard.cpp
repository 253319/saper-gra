//
// Created by c on 3/9/20.
//
#include <iostream>
#include "MinesweeperBoard.h"
#include <ctime>
#include <cstdlib>
#include <ctime>
using namespace std;
MinesweeperBoard::MinesweeperBoard(int x, int y, GameMode mode)
{
	this->width = x;
	this->height = y;
	this->level = mode;
	this->state = RUNNING;
	for (int i = 0; i < this->width; i++)
	{
		for (int j = 0; j < this->height; j++)
		{
			this->board[i][j].hasMine = false;
			this->board[i][j].hasFlag = false;
			this->board[i][j].isRevealed = false;
		}
	}
	int xLos = 0;
	int yLos = 0;
	if (this->level == EASY) this->mines = 0.1*(this->width*this->height);
	else if (this->level == NORMAL) this->mines = 0.2*(this->width*this->height);
	else if (this->level == HARD) this->mines = 0.3*(this->width*this->height);
	srand(time(NULL));
	if (this->level != DEBUG)
		for (int i = 0; i < this->mines;)
		{

			xLos = rand() % this->width;
			yLos = rand() % this->height;
			//cout << xLos <<" "<<yLos<< endl;
			if (this->board[xLos][yLos].hasMine == false)
			{
				this->board[xLos][yLos].hasMine = true;
				i++;
			}
		}
	else
		for (int i = 0; i < this->width; i++)
			for (int j = 0; j < this->height; j++)
			{
				if (i == j)
					this->board[i][j].hasMine = true;
				else if (i == 0)
					this->board[i][j].hasMine = true;
				else if (j == 0 && i % 2 == 0)
					this->board[i][j].hasMine = true;
			}
	/*	this->board[2][3].hasMine = true;
	this->board[6][8].hasFlag = true;
	this->board[2][7].isRevealed = true;*/
}
void MinesweeperBoard::debug_display() const
{
	for (int i = 0; i < this->width; i++)
	{
		for (int j = 0; j < this->height; j++)
		{
			cout << "[";
			if (this->board[i][j].hasMine == true)
				cout << "M";
			else
				cout << ".";
			if (this->board[i][j].hasFlag == true)
				cout << "f";
			else
				cout << ".";
			if (this->board[i][j].isRevealed == true)
				cout << "o";
			else
				cout << ".";
			cout << "]";
		}
		cout << endl;
	}
}
int MinesweeperBoard::getBoardWidth() const
{
	return this->width;
}
int MinesweeperBoard::getBoardHeight() const
{
	return this->height;
}
int MinesweeperBoard::getMineCount() const
{
	if (this->level == EASY)
		return 0.1*(this->width*this->height);
	else if (this->level == NORMAL)
		return 0.2*(this->width*this->height);
	else if (this->level == HARD)
		return 0.3*(this->width*this->height);
	else
		return -1;
}
int MinesweeperBoard::countMines(int x, int y) const
{
	int count = 0;
	if (x > this->width || y > this->height)
		return -1;
	else
	{
		if (this->board[x - 1][y].hasMine == true && x>0) count++;
		if (this->board[x + 1][y].hasMine == true && x<this->width) count++;
		if (this->board[x][y - 1].hasMine == true && y>0) count++;
		if (this->board[x][y + 1].hasMine == true && y<this->height) count++;
		if (this->board[x + 1][y - 1].hasMine == true && x<this->width && y>0) count++;
		if (this->board[x - 1][y - 1].hasMine == true && x>0 && y>0) count++;
		if (this->board[x + 1][y + 1].hasMine == true && x<this->width && y<this->height) count++;
		if (this->board[x - 1][y + 1].hasMine == true && x>0 && y<this->height) count++;
	}
	return count;
}
bool MinesweeperBoard::hasFlag(int x, int y) const
{
	if (this->board[x][y].hasFlag == false || x>this->getBoardWidth() || y>this->getBoardHeight() || this->board[x][y].isRevealed == true)
		return false;
	else return true;
}
void MinesweeperBoard::toogleFlag(int x, int y)
{
	if (!this->hasFlag(x, y) && this->state == RUNNING)
		this->board[x][y].hasFlag = true;
}
bool MinesweeperBoard::isRevealed(int x, int y) const
{
	return this->board[x][y].isRevealed;
}
void MinesweeperBoard::revealField(int x, int y)
{
	if (!this->isRevealed(x, y) && x <= this->getBoardWidth() && y <= this->getBoardHeight() && !this->hasFlag(x, y) && this->state == RUNNING)
	{
		if (this->board[x][y].hasMine == true)
		{
			this->board[x][y].isRevealed = true;
			this->state = FINISHED_LOSS;
		}
		else
		{
			this->board[x][y].isRevealed = true;
			int zlicz = 0;
			for (int i = 0; i < this->getBoardWidth(); i++)
				for (int j = 0; j < this->getBoardWidth(); j++)
					if (this->isRevealed(i, j) == true)zlicz++;
			if (this->getBoardHeight()*this->getBoardWidth() - zlicz == mines) this->state = FINISHED_WIN;

		}
	}
}
GameState MinesweeperBoard::getGameState() const
{
	return this->state;
}
char MinesweeperBoard::getFieldInfo(int x, int y) const
{
	if (x > this->width || y > this->height) return '#';
	else if (this->isRevealed(x, y) == false && this->hasFlag(x, y) == true) return 'F';
	else if (this->isRevealed(x, y) == false && this->hasFlag(x, y) == false) return '_';
	else if (this->isRevealed(x, y) == true && this->board[x][y].hasMine == true) return 'x';
	else if (this->isRevealed(x, y) == true && this->countMines(x, y) == 0) return ' ';
	else if (this->isRevealed(x, y) == true && this->countMines(x, y) != 0) return char(this->countMines(x, y) + 48);
}



