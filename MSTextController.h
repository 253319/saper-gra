#ifndef UNTITLED5_MSTEXTCONTROLLER_H
#define UNTITLED5_MSTEXTCONTROLLER_H
#include "MSBoardTextView.h"
#include "MinesweeperBoard.h"
class MSTextController{

public:
	MinesweeperBoard &board;
	MSBoardTextView &view;
	MSTextController(MinesweeperBoard &b, MSBoardTextView &v);
	void ctrl() const;
};
#endif