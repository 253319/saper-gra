#ifndef UNTITLED5_MSSFMLVIEW_H
#define UNTITLED5_MSSFMLVIEW_H
#include "MinesweeperBoard.h"
#include <SFML/Graphics.hpp>
class MSSFMLView{
	public:
		MinesweeperBoard &board;
		MSSFMLView(MinesweeperBoard &b);
		void view() const;
};
#endif