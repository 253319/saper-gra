#include "MSSFMLView.h"

using namespace std;
using namespace sf;
MSSFMLView::MSSFMLView(MinesweeperBoard &b) : board(b) {}

void MSSFMLView::view() const
{
	RenderWindow window(sf::VideoMode(160, 160), "Saper");
	Texture shape;
	Texture good;
	Texture good1;
	Texture good2;
	Texture good3;
	Texture good4;
	Texture good5;
	Texture flag;
	Texture fail;
	shape.loadFromFile("C:/Users/Kacper/Desktop/Saper/saper.jpg");
	good.loadFromFile("C:/Users/Kacper/Desktop/Saper/SaperGood.jpg");
	good1.loadFromFile("C:/Users/Kacper/Desktop/Saper/Saper1.jpg");
	good2.loadFromFile("C:/Users/Kacper/Desktop/Saper/Saper2.jpg");
	good3.loadFromFile("C:/Users/Kacper/Desktop/Saper/Saper3.jpg");
	good4.loadFromFile("C:/Users/Kacper/Desktop/Saper/Saper4.jpg");
	good5.loadFromFile("C:/Users/Kacper/Desktop/Saper/Saper5.jpg");
	fail.loadFromFile("C:/Users/Kacper/Desktop/Saper/SaperFail.jpg");
	flag.loadFromFile("C:/Users/Kacper/Desktop/Saper/SaperFlag.jpg");
	window.clear();
	Sprite s;
	s.setTexture(shape);
	window.draw(s);
	window.display();
	while (window.isOpen())
	{
		Vector2i pos = Mouse::getPosition(window);
		int xx = pos.x;
		xx = xx - xx % 16;
		int x = xx / 16;
		int yy = pos.y;
		yy = yy - yy % 16;
		int y = yy / 16;
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				window.close();
			if (event.type == Event::MouseButtonPressed)
			{
				if (event.mouseButton.button == Mouse::Right)
				{
					board.toogleFlag(x, y);
					window.clear();
					shape.update(flag, xx, yy);
					s.setTexture(shape);
					window.draw(s);
					window.display();

				}
				if (event.mouseButton.button == Mouse::Left)
				{
					board.revealField(x, y);
					if (board.getFieldInfo(x, y) == ' ')
					{
						window.clear();
						shape.update(good, xx, yy);
						s.setTexture(shape);
						window.draw(s);
						window.display();
					}
					else if (board.getFieldInfo(x, y) == '1')
					{
						window.clear();
						shape.update(good1, xx, yy);
						s.setTexture(shape);
						window.draw(s);
						window.display();
					}
					else if (board.getFieldInfo(x, y) == '2')
					{
						window.clear();
						shape.update(good2, xx, yy);
						s.setTexture(shape);
						window.draw(s);
						window.display();
					}
					else if (board.getFieldInfo(x, y) == '3')
					{
						window.clear();
						shape.update(good3, xx, yy);
						s.setTexture(shape);
						window.draw(s);
						window.display();
					}
					else if (board.getFieldInfo(x, y) == '4')
					{
						window.clear();
						shape.update(good4, xx, yy);
						s.setTexture(shape);
						window.draw(s);
						window.display();
					}
					else if (board.getFieldInfo(x, y) == '5')
					{
						window.clear();
						shape.update(good5, xx, yy);
						s.setTexture(shape);
						window.draw(s);
						window.display();
					}
					else if (board.getFieldInfo(x, y) == 'x')
					{
						window.clear();
						shape.update(fail, xx, yy);
						s.setTexture(shape);
						window.draw(s);
						window.display();
					}

				}
			}
		}

	}
}
