
#include "MSBoardTextView.h"

#include "MinesweeperBoard.h"

using namespace std;
MSBoardTextView::MSBoardTextView(MinesweeperBoard & b) : board(b) {}
void MSBoardTextView::display() const {
	for (int i = 0; i<board.getBoardWidth(); i++) {
		for (int j = 0; j<board.getBoardHeight(); j++) {
			cout << "[" << board.getFieldInfo(i, j) << "]";
		}
		cout << endl;
	}
}

