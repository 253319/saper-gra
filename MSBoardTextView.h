
//#include <iostream>
#ifndef UNTITLED5_MSBOARDTEXTVIEW_H
#define UNTITLED5_MSBOARDTEXTVIEW_H
#include "MinesweeperBoard.h"



class MSBoardTextView{
private:
	MinesweeperBoard &board;

public:
	explicit MSBoardTextView(MinesweeperBoard &b);
	void display() const;
};
#endif