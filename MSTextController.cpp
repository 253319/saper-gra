
 #include "MSTextController.h"
using namespace std;
MSTextController::MSTextController(MinesweeperBoard &b, MSBoardTextView &v) : board(b), view(v){}

void MSTextController::ctrl() const
{
	char bufor[10];
	int x;
	int y;
	while (board.getGameState() == RUNNING)
	{
		//system("cls");
		view.display();
		cout << endl;
		cout << "Aby zaznaczyc pole wpisz O(x,y), gdzie x i y to wspolrzedne punktu" << endl;
		cout << "Aby oflagowac pole wpisz F(x,y), gdzie x i y to wspolrzedne punktu" << endl;
		cin >> bufor[0] >> bufor[1] >> bufor[2] >> bufor[3] >> bufor[4] >> bufor[5];
		if (bufor[5] != ')')
		{
			cin >> bufor[6];
			if (bufor[6] != ')')
				cin >> bufor[7];
		}
		if (bufor[0] == 'O')
		{
			if (bufor[3] == ',')
			{
				x = bufor[2] - 48;
				if (bufor[5] == ')')
					y = bufor[4] - 48;
				else
					y = 10 * (bufor[4] - 48) + (bufor[5] - 48);
			}
			else
			{
				x = 10 * (bufor[2] - 48) + (bufor[3] - 48);
				if (bufor[6] == ')')
					y = bufor[5] - 48;
				else
					y = 10 * (bufor[5] - 48) + (bufor[6] - 48);
			}
			board.revealField(x, y);
		}
		else if (bufor[0] == 'F')
		{
			if (bufor[3] == ',')
			{
				x = bufor[2] - 48;
				if (bufor[5] == ')')
					y = bufor[4] - 48;
				else
					y = 10 * (bufor[4] - 48) + (bufor[5] - 48);
			}
			else
			{
				x = 10 * (bufor[2] - 48) + (bufor[3] - 48);
				if (bufor[6] == ')')
					y = bufor[5] - 48;
				else
					y = 10 * (bufor[5] - 48) + (bufor[6] - 48);
			}
			board.toogleFlag(x, y);
		}
		else
			cout << "Dane wprowadzone niepoprawnie" << endl;
	}
	if (board.getGameState() == FINISHED_WIN)
		cout << "Gratulacje! Wygrales";
	else
		cout << "Niestety przegrales gre";

}